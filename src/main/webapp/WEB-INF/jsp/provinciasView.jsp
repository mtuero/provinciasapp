<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/estilos.css" type="text/css">
        <title>Provincias App</title>
    </head>
    <body>
        <div class="container">
            <div class="row encabezado">
                <div class="col-xs-12">
                    <h1>Bienvenido a Provincias App!</h1>
                    <p>Provincias App es una aplicacion que te permite dar el alta, baja y modificacion de provincias.</p>
                </div>
            </div>
            <div class="row formulario">
                <hr>
                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <form class="form-horizontal" role="form" method="POST">
                            <div class="form-group">
                                <label>Ingresar nueva provincia</label>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-2 control-label">Nombre: </label>
                                <div class="col-xs-10">
                                    <input type="text" name="nombre" class="form-control" maxlength="30" placeholder="Ingrese nombre" required>
                                </div>                            
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 control-label">Cantidad de habitantes: </label>
                                <div class="col-xs-9">
                                    <input type="number" name="habitantes" class="form-control" placeholder="Ingrese cantidad">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-offset-10">
                                    <input type="submit" class="btn btn-primary" value="Guardar">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row tabla">
                <hr>
                <div class="row header-tabla">
                    <div class="col-xs-6">
                        Provincia
                    </div>
                    <div class="col-xs-6">
                        Cantidad de habitantes
                    </div>
                </div>
                <div class="col-xs-12">
                    <c:forEach var="provincia" items="${provincias}">
                        <div class="row registro">
                            <div class="col-xs-6">
                                ${provincia.nombre}
                            </div>
                            <div class="col-xs-6">
                                ${provincia.habitantes}
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </body>
</html>
