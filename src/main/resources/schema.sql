DROP TABLE IF EXISTS provincia;

CREATE TABLE provincia (
    id BIGINT PRIMARY KEY IDENTITY,
    nombre VARCHAR(30),
    habitantes BIGINT
);

INSERT INTO provincia (nombre,habitantes) VALUES ('buenos aires',100);
INSERT INTO provincia (nombre,habitantes) VALUES ('mendoza',50);