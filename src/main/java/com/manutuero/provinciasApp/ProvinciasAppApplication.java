package com.manutuero.provinciasApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvinciasAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvinciasAppApplication.class, args);
	}
}
