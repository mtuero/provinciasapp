package com.manutuero.provinciasApp.repository;

import com.manutuero.provinciasApp.domain.Provincia;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProvinciaRepository extends JpaRepository<Provincia, Long>{
    
}
