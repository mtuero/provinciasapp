package com.manutuero.provinciasApp.controller;

import com.manutuero.provinciasApp.domain.Provincia;
import com.manutuero.provinciasApp.service.ProvinciaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProvinciaController {

    @Autowired
    private ProvinciaService provinciaService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
        model.addAttribute("provincias", provinciaService.buscarTodas());
        return "provinciasView";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String guardar(Model model,
        @RequestParam(name = "nombre") String nombre,
        @RequestParam(name = "habitantes") Long habitantes) {
        
        long id = provinciaService.buscarTodas().size()+1;
        
        Provincia provincia = new Provincia();
        provincia.setId(id);
        provincia.setNombre(nombre);
        provincia.setHabitantes(habitantes);

        provinciaService.guardar(provincia);
        
        model.addAttribute("provincias", provinciaService.buscarTodas());
        return "provinciasView";
    }
}
