package com.manutuero.provinciasApp.service.impl;

import com.manutuero.provinciasApp.domain.Provincia;
import com.manutuero.provinciasApp.repository.ProvinciaRepository;
import com.manutuero.provinciasApp.service.ProvinciaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ProvinciaServiceImpl implements ProvinciaService {

    @Autowired
    private ProvinciaRepository provinciaRepository;

    @Override
    public List<Provincia> buscarTodas() {
        return provinciaRepository.findAll();
    }

    @Override
    public Provincia buscarPorId(Long id) {
        return provinciaRepository.findOne(id);
    }

    @Override
    public void guardar(Provincia provincia) {
        if (provincia.getId() == null) {
            throw new IllegalArgumentException("Campo id no debe ser nulo");
        }
        if (provincia.getNombre() == null || provincia.getNombre().trim().isEmpty()
            || !provincia.getNombre().matches("([a-z]|[A-Z]|\\s)+")) {
            throw new IllegalArgumentException("Campo nombre no admite null o vacio, ni debe contener numeros o caracteres especiales");
        }

        provinciaRepository.save(provincia);
    }
}
