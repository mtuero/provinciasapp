package com.manutuero.provinciasApp.service;

import com.manutuero.provinciasApp.domain.Provincia;
import java.util.List;

public interface ProvinciaService {
   
    List <Provincia> buscarTodas();
    
    Provincia buscarPorId(Long id);
    
    void guardar(Provincia provincia);
}
