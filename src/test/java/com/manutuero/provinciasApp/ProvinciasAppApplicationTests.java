package com.manutuero.provinciasApp;

import com.manutuero.provinciasApp.domain.Provincia;
import com.manutuero.provinciasApp.repository.ProvinciaRepository;
import com.manutuero.provinciasApp.service.ProvinciaService;
import javax.transaction.Transactional;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import static org.junit.Assert.assertEquals;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ProvinciasAppApplication.class)
@WebAppConfiguration
public class ProvinciasAppApplicationTests {

    @Autowired
    private ProvinciaService provinciaService;

    @Autowired
    private ProvinciaRepository provinciaRepository;

    @Test
    public void buscarTodas_provinciasExistentes_devuelveProvincias() {
        assertEquals(2, provinciaService.buscarTodas().size());
    }

    @Test
    public void guardar_provinciaConParametrosValidos_guardaNueva() {
        Provincia provincia = new Provincia();
        provincia.setId(3L);
        provincia.setNombre("chaco");
        provincia.setHabitantes(20L);

        provinciaService.guardar(provincia);

        assertEquals(3, provinciaRepository.findAll().size());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void guardar_provinciaConIdNull_lanzaExcepcion(){
        Provincia provincia = new Provincia();
        provincia.setId(null);
        provincia.setNombre("chaco");
        provincia.setHabitantes(20L);
        
        provinciaService.guardar(provincia);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void guardar_provinciaConNombreVacio_lanzaExcepcion(){
        Provincia provincia = new Provincia();
        provincia.setId(3L);
        provincia.setNombre("");
        provincia.setHabitantes(20L);
        
        provinciaService.guardar(provincia);
    }
        @Test(expected = IllegalArgumentException.class)
    public void guardar_provinciaConNombreNull_lanzaExcepcion(){
        Provincia provincia = new Provincia();
        provincia.setId(3L);
        provincia.setNombre(null);
        provincia.setHabitantes(20L);
        
        provinciaService.guardar(provincia);
    }
}
